package com.acmebank.accountmanager.service;

import com.acmebank.accountmanager.exception.AccountNotFound;
import com.acmebank.accountmanager.exception.BalanceNotEnough;
import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.AccountBalance;
import com.acmebank.accountmanager.model.Amount;
import com.acmebank.accountmanager.repository.AccountRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @InjectMocks
    private AccountServiceImpl accountService;

    @Mock
    private AccountRepository accountRepository;


    @BeforeEach
    void setUp() {

        accountRepository = Mockito.mock(AccountRepository.class);
        accountService = new AccountServiceImpl(accountRepository);

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllAccountBalance_Positive() {


        Account ac1 = new Account("11112222", new BigDecimal(2000));
        Account ac2 = new Account("33334444", new BigDecimal(5000));
        Account ac3 = new Account("55556666", new BigDecimal(8000));

        when(accountRepository.findAll())
                .thenReturn(Arrays.asList(ac1, ac2, ac3));

        List<AccountBalance> accountBalanceList = accountService.getAllAccountBalance();


        Assertions.assertEquals(3, accountBalanceList.size());

        Assertions.assertEquals("11112222", accountBalanceList.get(0).getAccountNumber());
        Assertions.assertEquals("HKD", accountBalanceList.get(0).getBalance().getCurrency());
        Assertions.assertEquals(new BigDecimal(2000), accountBalanceList.get(0).getBalance().getValue());

        Assertions.assertEquals("33334444", accountBalanceList.get(1).getAccountNumber());
        Assertions.assertEquals("HKD", accountBalanceList.get(1).getBalance().getCurrency());
        Assertions.assertEquals(new BigDecimal(5000), accountBalanceList.get(1).getBalance().getValue());

        Assertions.assertEquals("55556666", accountBalanceList.get(2).getAccountNumber());
        Assertions.assertEquals("HKD", accountBalanceList.get(2).getBalance().getCurrency());
        Assertions.assertEquals(new BigDecimal(8000), accountBalanceList.get(2).getBalance().getValue());

    }

    @Test
    void getAllAccountBalance_Empty() {

        when(accountRepository.findAll())
                .thenReturn(List.of());

        List<AccountBalance> accountBalanceList = accountService.getAllAccountBalance();

        Assertions.assertEquals(0, accountBalanceList.size());
    }

    @Test
    void getAccountBalanceByAccountNumber_Positive() {


        Account ac1 = new Account("11112222", new BigDecimal(2000));
        Account ac2 = new Account("33334444", new BigDecimal(5000));
        Account ac3 = new Account("55556666", new BigDecimal(8000));

        when(accountRepository.findById(ac1.getAccountNumber()))
                .thenReturn(Optional.of(ac1));
        when(accountRepository.findById(ac2.getAccountNumber()))
                .thenReturn(Optional.of(ac2));
        when(accountRepository.findById(ac3.getAccountNumber()))
                .thenReturn(Optional.of(ac3));


        AccountBalance expectedAccountBalance1 = new AccountBalance("11112222", new Amount("HKD", new BigDecimal(2000)));
        AccountBalance expectedAccountBalance2 = new AccountBalance("33334444", new Amount("HKD", new BigDecimal(5000)));
        AccountBalance expectedAccountBalance3 = new AccountBalance("55556666", new Amount("HKD", new BigDecimal(8000)));

        AccountBalance actualAccountBalance1 = accountService.getAccountBalanceByAccountNumber("11112222");
        AccountBalance actualAccountBalance2 = accountService.getAccountBalanceByAccountNumber("33334444");
        AccountBalance actualAccountBalance3 = accountService.getAccountBalanceByAccountNumber("55556666");

        Assertions.assertEquals(expectedAccountBalance1.getBalance().getValue(), actualAccountBalance1.getBalance().getValue());
        Assertions.assertEquals(expectedAccountBalance1.getBalance().getCurrency(), actualAccountBalance1.getBalance().getCurrency());
        Assertions.assertEquals(expectedAccountBalance1.getAccountNumber(), actualAccountBalance1.getAccountNumber());

        Assertions.assertEquals(expectedAccountBalance2.getBalance().getValue(), actualAccountBalance2.getBalance().getValue());
        Assertions.assertEquals(expectedAccountBalance2.getBalance().getCurrency(), actualAccountBalance2.getBalance().getCurrency());
        Assertions.assertEquals(expectedAccountBalance2.getAccountNumber(), actualAccountBalance2.getAccountNumber());

        Assertions.assertEquals(expectedAccountBalance3.getBalance().getValue(), actualAccountBalance3.getBalance().getValue());
        Assertions.assertEquals(expectedAccountBalance3.getBalance().getCurrency(), actualAccountBalance3.getBalance().getCurrency());
        Assertions.assertEquals(expectedAccountBalance3.getAccountNumber(), actualAccountBalance3.getAccountNumber());

    }

    @Test
    void getAccountBalanceByAccountNumber_NotFoundException() {

        Exception exception = Assertions.assertThrows(AccountNotFound.class, () -> {
            AccountBalance notFoundBalance = accountService.getAccountBalanceByAccountNumber("99999999");
        });

        String expectedMessage = "99999999 not found";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void transferAmount_Positive() {

        Account ac1 = new Account("11112222", new BigDecimal(2000));
        Account ac2 = new Account("33334444", new BigDecimal(5000));

        when(accountRepository.findById(ac1.getAccountNumber()))
                .thenReturn(Optional.of(ac1));
        when(accountRepository.findById(ac2.getAccountNumber()))
                .thenReturn(Optional.of(ac2));
        when(accountRepository.saveAll(Arrays.asList(ac1, ac2)))
                .thenReturn(Arrays.asList(
                        new Account("11112222", new BigDecimal(1000)),
                        new Account("33334444", new BigDecimal(6000))
                ));

        List<Account> resultAccounts = accountService.transferAmount("11112222", "33334444", new BigDecimal(1000));

        Assertions.assertEquals(2, resultAccounts.size());
        Assertions.assertEquals("11112222", resultAccounts.get(0).getAccountNumber());
        Assertions.assertEquals("HKD", resultAccounts.get(0).getCurrency());
        Assertions.assertEquals(new BigDecimal(1000), resultAccounts.get(0).getValue());
        Assertions.assertEquals("33334444", resultAccounts.get(1).getAccountNumber());
        Assertions.assertEquals("HKD", resultAccounts.get(1).getCurrency());
        Assertions.assertEquals(new BigDecimal(6000), resultAccounts.get(1).getValue());



    }
    @Test
    void transferAmount_BalanceNotEnough() {

        Exception exception = Assertions.assertThrows(BalanceNotEnough.class, () -> {

            Account ac1 = new Account("11112222", new BigDecimal(2000));
            Account ac2 = new Account("33334444", new BigDecimal(5000));

            when(accountRepository.findById(ac1.getAccountNumber()))
                    .thenReturn(Optional.of(ac1));
            when(accountRepository.findById(ac2.getAccountNumber()))
                    .thenReturn(Optional.of(ac2));

            List<Account> resultAccounts = accountService.transferAmount("11112222", "33334444", new BigDecimal(3000));

        });

        String expectedMessage = "Account (11112222) balance not enough: 2000";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(expectedMessage, actualMessage);


    }
}