package com.acmebank.accountmanager;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaAuditing
@EnableJpaRepositories
public class AccountManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountManagerApplication.class, args);
    }

    @Bean
    public OpenAPI acmeBankAccountManagerApi() {
        return new OpenAPI()
                .info(new Info().title("ACME Bank Account Manager API")
                        .description("This API spec serves for ACME Bank Account Manager")
                        .version("v1.0").contact(
                                new Contact()
                                        .url("https://github.com/ryanleodev")
                                        .email("ctleeab@connect.ust.hk")
                                        .name("Ryan Lee")
                        ));
    }
}
