package com.acmebank.accountmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.math.BigDecimal;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BalanceNotEnough extends RuntimeException {

    public BalanceNotEnough(String accountNumber, BigDecimal balance) {
        super("Account (" + accountNumber + ") balance not enough: " + balance);
    }

}
