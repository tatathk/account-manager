package com.acmebank.accountmanager.controller;

import com.acmebank.accountmanager.exception.AccountNotFound;
import com.acmebank.accountmanager.exception.BalanceNotEnough;
import com.acmebank.accountmanager.model.AccountManagerError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Slf4j
public class AccountManagerControllerAdvice {

    @ExceptionHandler(AccountNotFound.class)
    public ResponseEntity<AccountManagerError> handleAccountNotFoundException(
            AccountNotFound accountNotFound,
            WebRequest request
    ) {
        log.error("AccountNotFound", accountNotFound.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new AccountManagerError("ACCOUNT_NOT_FOUND", accountNotFound.getMessage()));


    }

    @ExceptionHandler(BalanceNotEnough.class)
    public ResponseEntity<AccountManagerError> handleBalanceNotEnoughException(
            BalanceNotEnough balanceNotEnough,
            WebRequest request
    ) {
        log.error("Balance Not Enough", balanceNotEnough.getMessage());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new AccountManagerError("BALANCE_NOT_ENOUGH", balanceNotEnough.getMessage()));

    }

}
