package com.acmebank.accountmanager.controller;

import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.AccountBalance;
import com.acmebank.accountmanager.model.AccountTransferRequest;
import com.acmebank.accountmanager.model.AccountTransferResponse;
import com.acmebank.accountmanager.service.AccountService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

import static com.acmebank.accountmanager.constant.AccountConstant.ACCOUNT_NUMBER_PATTERN;

@RestController
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/accounts")
    @Operation(summary = "List all accounts with balance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {
                            @Content(array = @ArraySchema(schema = @Schema(implementation = AccountBalance.class)))
                    })
    })
    public List<AccountBalance> getAllAccountBalance() {
        return accountService.getAllAccountBalance();
    }

    @GetMapping("/accounts/{accountNumber}")
    @Operation(summary = "Get account with balance by account number")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {
                            @Content(array = @ArraySchema(schema = @Schema(implementation = AccountBalance.class)))
                    })
    })
    public AccountBalance getAccountBalance(@PathVariable @NotBlank @Pattern(regexp = ACCOUNT_NUMBER_PATTERN) String accountNumber) {
        return accountService.getAccountBalanceByAccountNumber(accountNumber);
    }

    @PostMapping("/accounts/{sourceAccountNumber}/payments")
    @Operation(summary = "Proceed payment transfer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {
                            @Content(array = @ArraySchema(schema = @Schema(implementation = AccountTransferResponse.class)))
                    })
    })
    public AccountTransferResponse createTransferPayment(@PathVariable("sourceAccountNumber") @NotBlank @Pattern(regexp = ACCOUNT_NUMBER_PATTERN) String accountNumber,
                                                  @RequestBody AccountTransferRequest accountPaymentRequest) {
        String targetAccountNumber = accountPaymentRequest.getTargetAccountNumber();
        List<Account> accounts = accountService.transferAmount(accountNumber, targetAccountNumber, accountPaymentRequest.getAmountValue());
        return new AccountTransferResponse(accountNumber, targetAccountNumber, accounts != null && !accounts.isEmpty());
    }

}
