package com.acmebank.accountmanager.repository;

import com.acmebank.accountmanager.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface AccountRepository extends JpaRepository<Account, String> {

}
