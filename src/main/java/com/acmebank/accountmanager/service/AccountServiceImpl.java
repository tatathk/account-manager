package com.acmebank.accountmanager.service;

import com.acmebank.accountmanager.exception.AccountNotFound;
import com.acmebank.accountmanager.exception.BalanceNotEnough;
import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.AccountBalance;
import com.acmebank.accountmanager.model.Amount;
import com.acmebank.accountmanager.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<AccountBalance> getAllAccountBalance() {
        return this.accountRepository.findAll().stream()
                .map(account -> new AccountBalance(
                        account.getAccountNumber(),
                        new Amount().setCurrency(account.getCurrency()).setValue(account.getValue())
                )).collect(Collectors.toList());
    }

    @Override
    public AccountBalance getAccountBalanceByAccountNumber(String accountNumber) {
        return this.accountRepository.findById(accountNumber).map(account -> new AccountBalance(
                account.getAccountNumber(),
                new Amount().setCurrency(account.getCurrency()).setValue(account.getValue())
        )).orElseThrow(() -> new AccountNotFound(accountNumber));
    }

    @Override
    public List<Account> transferAmount(String sourceAccountNumber, String targetAccountNumber, BigDecimal amount) {

        Account sourceAccount = this.accountRepository.findById(sourceAccountNumber)
                .orElseThrow(() -> new AccountNotFound(sourceAccountNumber));

        Account targetAccount = this.accountRepository.findById(targetAccountNumber)
                .orElseThrow(() -> new AccountNotFound(targetAccountNumber));


        BigDecimal sourceAccountValue = sourceAccount.getValue();
        BigDecimal targetAccountValue = sourceAccount.getValue();

        log.info("Current source balance: {}, target balance: {}, transferAmount: {}",  sourceAccountValue, targetAccountValue, amount);

        if (sourceAccountValue.compareTo(amount) < 0) {
            throw new BalanceNotEnough(sourceAccount.getAccountNumber(), sourceAccount.getValue());
        }

        BigDecimal newTargetAccountValue = targetAccount.getValue().add(amount);
        BigDecimal newSourceAccountValue = sourceAccountValue.subtract(amount);

        sourceAccount.setValue(newSourceAccountValue);
        targetAccount.setValue(newTargetAccountValue);

        return this.accountRepository.saveAll(Arrays.asList(
                sourceAccount, targetAccount
        ));
    }
}
