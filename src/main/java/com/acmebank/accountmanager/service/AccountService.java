package com.acmebank.accountmanager.service;

import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.AccountBalance;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    List<AccountBalance> getAllAccountBalance();

    AccountBalance getAccountBalanceByAccountNumber(String accountNumber);

    @Transactional
    List<Account> transferAmount(String sourceAccountNumber, String targetAccountNumber, BigDecimal amount);

}
