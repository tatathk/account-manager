package com.acmebank.accountmanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class AccountTransferResponse {
    private String accountNumber;
    private String targetAccountNumber;
    private boolean success;
}
