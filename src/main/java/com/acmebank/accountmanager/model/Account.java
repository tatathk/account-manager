package com.acmebank.accountmanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    @Id
    private String accountNumber;

    @Column(name = "value", nullable = false)
    private BigDecimal value;

    @Column(name = "currency", nullable = false)
    private String currency;

    @Version
    private Long version;

    public Account(String accountNumber, BigDecimal value) {
        this.accountNumber = accountNumber;
        this.value = value;
        this.currency = "HKD";
    }

}
