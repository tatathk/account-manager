package com.acmebank.accountmanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class AccountTransferRequest {
    private String targetAccountNumber;
    private BigDecimal amountValue;
}
