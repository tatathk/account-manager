package com.acmebank.accountmanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class AccountBalance {
    private String accountNumber;
    private Amount balance;

    public AccountBalance(String accountNumber, BigDecimal value) {
        this.accountNumber = accountNumber;
        this.balance = new Amount("HKD", value);
    }
}
