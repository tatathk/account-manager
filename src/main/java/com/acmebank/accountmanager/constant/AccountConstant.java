package com.acmebank.accountmanager.constant;

public class AccountConstant {

    public static final String ACCOUNT_NUMBER_PATTERN = "^\\d{8}$";
}
