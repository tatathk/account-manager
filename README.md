# 1. Introduction

1. This project is using Spring Boot 2 with H2 Database, Hibernate, Lombok, Flyway and SpringDoc (for OpenAPI 3 Documentation)
2. Please note build requires JDK 11
3. The default port is 8080 you can override by passing argument `-Dserver.port=XXXX` in `java` command
4. The file location of H2 Database is on current working directory `./account-db.mv.db`; Please grant correct access to the file to avoid permission denied error.

# 2. Getting Started

1. [Build] You can build the code with gradle `gradle clean build` or gradle wrapper `./gradlew clean build`

2. [Run] You can run the code with `java -jar ./build/libs/accountmanager-1.0.jar`

# 3. Function Endpoint
1. [List all accounts with balance] GET /accounts 
2. [Get account with balance] GET /accounts/{accountNumber}
3. [Create payment transfer] POST /accounts/{sourceAccountNumber}/payments

# 4. Tooling Endpoint
1. Swagger UI: http://{{host}}/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
2. Health Endpoint: http://{{host}}/actuator/health
